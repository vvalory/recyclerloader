package com.batrakov.loader_recycler

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.batrakov.loader_recycler.databinding.ActivityFilmItemBinding
import com.batrakov.loader_recycler.databinding.ActivityLoaderBinding
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class FilmAdapter(var list: MutableList<FilmData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class FilmHolder(val bind: ActivityFilmItemBinding) : RecyclerView.ViewHolder(bind.root)
    inner class LoaderHolder(bind: ActivityLoaderBinding) : RecyclerView.ViewHolder(bind.root)

    var isLoading = false
    var page = 1

    override fun getItemViewType(position: Int): Int {
        if (position >= list.size) {
            if (!isLoading && page != -1){
                getData(page, this)
                isLoading = true
            }
            return 1

        }else{
            return 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val bind = ActivityFilmItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val bind_loader = ActivityLoaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return when (viewType){
            0 -> FilmHolder(bind)
            else -> LoaderHolder(bind_loader)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FilmHolder){
            if (list[0].items[position].nameOriginal !== null){
                holder.bind.filmName.text = list[0].items[position].nameOriginal
            }else if (list[0].items[position].nameRu !== null){
                holder.bind.filmName.text = list[0].items[position].nameRu
            }else{
                holder.bind.filmName.text = "No Name"
            }

            holder.bind.filmType.text = "Type: ${list[0].items[position].type}"
            holder.bind.filmYear.text = "Year: ${list[0].items[position].year}"
            holder.bind.filmRating.text = "IMDB: ${list[0].items[position].ratingImdb}"
            holder.bind.image.load("${list[0].items[position].posterUrl}")
        }
    }

    override fun getItemCount() = list.size + 1

    fun update(items: List<FilmData>){
        list += items
        if (items.isNotEmpty()){
            page += 1
        }else{
            page = -1
        }
        notifyDataSetChanged()
        isLoading = false
    }





    fun getData(page: Int, recyclerAdapter: FilmAdapter){
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val http: HttpResponse = HttpClient().request {
                    url("https://kinopoiskapiunofficial.tech/api/v2.2/films")
                    parameter("page", page)
                    header("X-API-KEY", "a7fd7151-cbf2-4f5d-ac05-603e5906fb05")
                    method = HttpMethod.Get
                }
                if (http.status == HttpStatusCode.OK){
                    val data = Gson().fromJson(http.bodyAsText(), FilmData::class.java)

                    Log.i("1NET", data.toString())

                    CoroutineScope(Dispatchers.Main).launch {
                    recyclerAdapter.update(mutableListOf(data))
                    }
                }

            }catch (e: Exception){}


        }
    }


}