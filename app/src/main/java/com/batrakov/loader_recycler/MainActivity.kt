package com.batrakov.loader_recycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.batrakov.loader_recycler.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var bind: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bind.root)

        bind.recyclerView.adapter = FilmAdapter(mutableListOf())

    }
}